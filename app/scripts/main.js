
$.validate({
    modules : 'html5, security',
    inputParentClassOnSuccess: 'has-success',
    
});

$(document).ready(function() {


  $('.ajax').blur(function(e) {
    e.preventDefault();
    // Remove old errors
    if ($(this).hasClass('valid')) {
      $(this).next('.help-block').remove();

      $.ajax({
        type: 'POST',
        url: '/user/ajax_check',
        dataType: 'json',
        data: { name: $(this).attr('id'), value: $(this).val() },
        success: function(data) {
          // Insert new messages
          for (var key in data) {
            var id = '#' + key;
            var msg = '';
            if (data[key]) {

              for (var i in data[key]) {
                msg += '<span class="help-block form-error">' + data[key][i] + '</span>';
              }
            }
            $(id).after(msg);

          }
        }

      });
    }

  }); 
});


// Uncomment to enable Bootstrap tooltips
// https://getbootstrap.com/docs/4.0/components/tooltips/#example-enable-tooltips-everywhere
// $(function () { $('[data-toggle="tooltip"]').tooltip(); });

// Uncomment to enable Bootstrap popovers
// https://getbootstrap.com/docs/4.0/components/popovers/#example-enable-popovers-everywhere
// $(function () { $('[data-toggle="popover"]').popover(); });
